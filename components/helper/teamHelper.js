export const lead = {
    name : "Ayushman Kumar",
    image : "https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fayushma.jpg?alt=media&token=c47871f1-5641-4244-9ed0-27f31db1811d",
    linkedin : "https://www.linkedin.com/in/ayushman-kumar-836a19190/",
    github : ' https://github.com/ayushmankumar7',
    pos : "DSC Lead"
}

export const coreLeads = [
    {
        name : 'Aritra Bhattacharjee',
        github : 'https://github.com/Radiumskull',
        linkedin : 'https://www.linkedin.com/in/aritra-bhattacharjee-b6b41619',
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Faritra.jpg?alt=media&token=b1c232c1-877d-4d26-bb5d-6d038a81d253',
        pos : 'Web Lead'
    },
    {
        name : "Mayukh Chakraborty",
        linkedin : "https://www.linkedin.com/in/mayukh-chakraborty-22bb23165/",
        github : " https://github.com/Mayukh0",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fmayukh.jpg?alt=media&token=878730f5-92d9-4ba5-aafb-ad23922fefec',
        pos : "Event Lead"
    },
    {
        name : "Pritam Ghosh",
        linkedin : "https://www.linkedin.com/in/pritamgh",
        github : "https://github.com/mrgpritam",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fpritam.jpg?alt=media&token=5e7951f0-418b-47bf-beef-bcfb412c3ff2',
        pos : "Design Lead"
    }
]

export const coreMembers = [
    {
        linkedin : "https://www.linkedin.com/in/n-sai-shashank-58034a159",
        github : "https://github.com/shashank030401",
        name : "N Sai Shashank",
        pos : "Web Team",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fshashank.jpg?alt=media&token=077b8eaf-a988-4886-8327-a520f784d22a'

    },
    {
        name : "Reetam Dutta",
        linkedin : "https://www.linkedin.com/in/reetamdutta1/",
        github : "https://github.com/reetamdutta1",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Freetam.jpg?alt=media&token=6e9e455a-f989-4be9-b093-5d202e04998a',
        pos : "Design Team"
    }, 
    {
        linkedin : "https://www.linkedin.com/in/disha-kapoor-780a65187/",
        github : "https://github.com/dishak331",
        name : "Disha Kapoor",
        pos : "Management Team",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fdisha.jpg?alt=media&token=266d8a72-fc4b-4bc9-94e8-4f3f23e18a4a'

    }, 
    {
        name : "Anjali Jain",
        linkedin : "https://www.linkedin.com/in/anjali-jain-065bb41a0/",
        github : "https://github.com/Anjalijain29",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fanjali.jpg?alt=media&token=9f5050aa-1988-4137-a951-3337911bc6b7',
        pos : "Management Team"
    },
    {
        name : "Raunak Banerjee",
        linkedin : "https://www.linkedin.com/in/raunak-banerjee-677a661b0",
        github : "https://github.com/Raunak1003",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fraunak.jpg?alt=media&token=e62c08cf-97a2-4ac8-b073-92e45eff16da',
        pos : "Event Team"
    },
    {
        name : "Anumita Basak",
        linkedin : "www.linkedin.com/in/anumita-basak-8a3b9ca6",
        github : "https://github.com/Anumita01",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fanumita.jpg?alt=media&token=d81c23b6-0667-4dab-b180-a7a9261498cd',
        pos : "Event Team"
    },
    {
        name : "Bhawna Bharat Mehbubani",
        linkedin : "https://www.linkedin.com/in/bhawna-mehbubani-3445b4195/",
        github : "https://github.com/BhawnaMehbubani",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fbhawna.jpg?alt=media&token=090132d5-10f1-45e8-9c41-5be18581dbda',
        pos : "Outreach Team"
    },
    {
        name : "Archana Kumari Prajapati",
        linkedin : "https://www.linkedin.com/in/archana-prajapati-42a0251b2",
        github : "https://github.com/archana0708",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Farchanaprjapati.jpg?alt=media&token=83744e90-e02e-4ca1-8cd9-298fe31f2479',
        pos : "Event Team"
    },
    {
        name : "Akashdeep Bhattacharya",
        linkedin : "https://www.linkedin.com/in/akashdeep-bhattacharya-8aa417158/",
        github : "https://github.com/akashdeep007",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fakashdeep.jpg?alt=media&token=d5cf4e66-086d-40cb-8d94-f5d9387c16dc',
        pos : "Management Team"
    },
    {
        name : "Tanushree Shaw",
        linkedin : "https://www.linkedin.com/in/tanushree-shaw-438ab4193/",
        github : "https://github.com/TanushreeShaw",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Ftanushree.jpg?alt=media&token=5c1b7fce-ebaf-4ccf-a8d3-89eaaf299090',
        pos : "Web Team"
    },
    {
        name : "Pramita Barik",
        linkedin : "https://www.linkedin.com/in/pramita-barik-94b3331a8",
        github : "https://github.com/pramita0400",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fpranita.jpg?alt=media&token=d9721b81-7063-44a1-bf33-859ceb438bd0',
        pos : "Design Team"
    },
    {
        name : "Vivek Agarwal",
        linkedin : "https://www.linkedin.com/mwlite/in/vivek-agarwal-268a841a0",
        github : "https://github.com/Vivek798",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fvivek.jpg?alt=media&token=766fe8e2-29e4-464a-94ae-76ebc3682277',
        pos : "Outreach Team"
    },    {
        name : "Debojyoti Mallick",
        linkedin : "https://www.linkedin.com/in/debojyoti-mallick-7aaa741a0/",
        github : "https://github.com/debojyoti777",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fdebojyoti.jpg?alt=media&token=582758a2-9106-449f-b48a-c46f5ed235a2',
        pos : "Outreach Team"
    },
    {
        name : "Parichay Dey",
        linkedin : "https://www.linkedin.com/in/parichay-dey-67291419a/",
        github : "https://github.com/ParichayDey8",
        image : 'https://firebasestorage.googleapis.com/v0/b/dscbppimt-cms-storage.appspot.com/o/member_images%2Fparichay.jpg?alt=media&token=5cbf46a9-c21f-4583-b160-50d31a7c2d29',
        pos : "Management Team"
    }
]