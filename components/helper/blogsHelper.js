import placeholder from "./../../public/images/placeholder.jpg"


const blogs = [
{
    image: placeholder,
    platform : 'Medium',
    title: "App Development Using Flutter",
    language: "English",
    speaker: "shashank",
    discription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet justo maximus, tristique lorem sed, facilisis nulla. Nunc ac iaculis massa. Integer ultricies sodales turpis, et convallis dui lacinia sit amet. Duis placerat semper ante ac tempus. In hac habitasse platea dictumst. Vivamus pharetra scelerisque ex at iaculis. Quisque quis elit non nisl bibendum lobortis in ac sem. Sed vestibulum, quam eu maximus aliquam, nibh turpis facilisis sem, a lacinia lacus nibh eu felis."

},
{
    image: placeholder,
    title: "App Development Using Flutter",
    language: "English",
    platform : 'Medium',
    speaker: "shashank",
    discription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet justo maximus, tristique lorem sed, facilisis nulla. Nunc ac iaculis massa. Integer ultricies sodales turpis, et convallis dui lacinia sit amet. Duis placerat semper ante ac tempus. In hac habitasse platea dictumst. Vivamus pharetra scelerisque ex at iaculis. Quisque quis elit non nisl bibendum lobortis in ac sem. Sed vestibulum, quam eu maximus aliquam, nibh turpis facilisis sem, a lacinia lacus nibh eu felis."

},
{
    image: placeholder,
    title: "App Development Using Flutter",
    language: "English",
    platform : 'YouTube',
    speaker: "shashank",
    discription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet justo maximus, tristique lorem sed, facilisis nulla. Nunc ac iaculis massa. Integer ultricies sodales turpis, et convallis dui lacinia sit amet. Duis placerat semper ante ac tempus. In hac habitasse platea dictumst. Vivamus pharetra scelerisque ex at iaculis. Quisque quis elit non nisl bibendum lobortis in ac sem. Sed vestibulum, quam eu maximus aliquam, nibh turpis facilisis sem, a lacinia lacus nibh eu felis."

}]

export default blogs